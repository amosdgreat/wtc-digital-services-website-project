<?php

// var_dump(DB_NAME, DB_USER, DB_PASSWORD, DB_HOST);

// echo DB_NAME;



class SaneDb
{
    private $_oDb;

    public function __construct(wpdb $oDb)
    {
        $this->_oDb = $oDb;
    }

    public function __get($sField)
    {
        if($sField != '_oDb')
            return $this->_oDb->$sField;
    }

    public function __set($sField, $mValue)
    {
        if($sField != '_oDb')
            $this->_oDb->$sField = $mValue;
    }

    public function __call($sMethod, array $aArgs)
    {
        return call_user_func_array(array($this->_oDb, $sMethod), $aArgs);
    }

    public function getDbName() { echo $this->_oDb->dbname;     }
    public function getDbPass() { echo  $this->_oDb->dbpassword; }
    public function getDbHost() { echo  $this->_oDb->dbhost;     }
}




// global $sanedb;
// $sanedb = new SaneDb($wpdb);



// var_dump($sanedb);