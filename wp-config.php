<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wtc_digital' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lKiP~y@YP]tpK;`H%07U:|Y5l^S^anNjWd@v6o!@{wV8K=wH9hMeY7K3*BJ{`mCB' );
define( 'SECURE_AUTH_KEY',  'v=OLY<I8kf6 rz=FfF#(Z[Ka>$DF~X12pCun<kEr=*zG5)QNyZm#p8xqP} ~Awnm' );
define( 'LOGGED_IN_KEY',    'CNaou9bw+trntQJSP@&0x>](mEx4YeUX{|3<zko+7Y=pM48BgBC_2Zc]:fp8P=uj' );
define( 'NONCE_KEY',        'DGdxD`s2BD[KZ|a0WYd?MRm;PZ|G)=I~xKtR&d[zFzCHTPG)cI|fpu#)|%3M!C7A' );
define( 'AUTH_SALT',        '|c}bl3V!Omnu0q{A&d~ZeT^}!1USG]6~ao_ORQm41-/~&gWL~BKgRUR8o:-{ ?VW' );
define( 'SECURE_AUTH_SALT', 'iiv>[n@T/kT<q9&4n)BZ9}bT%.x|]Rk5jRb#!>(<XLK-/S+R__WgW.Ar#%8HO!3j' );
define( 'LOGGED_IN_SALT',   '6n^IEz7mN8vXfmLOY`[KZExR!vHYk|/b44ZU37zT1=.j6;u]{ ;qE*L=_A .kM^N' );
define( 'NONCE_SALT',       '?x>2qqeWr@Mm:AAaeo)TPBjHf?$8{BSmdRj8jA/NZfX^!7;,,e=g:ti={_r7Ip8c' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
